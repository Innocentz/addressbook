Requirements:
1) node version 10.13.0
2) npm v6.4.1
3) Angular6
4) SQL Server localDB/ Express
5) .NET Core 2.2

Instructions:

1) clone the repo from: https://gitlab.com/Innocentz/addressbook.git
2) Create an AddressBook database on localDB/ or any sql instance.
3) Run the addressbook.create.sql script to create the database objects and some dummy data.
4) open the AddressBook.API project and change the connection string to your database's connection.
5) Run the API locally
6) Open Command prompt and cd to the ../AddressBook/AddressBook.FrontEnd
7) Run npm install.

The application should show a few contacts with options to edit, delete, view and on the main panel should be able to add new contacts and search.

