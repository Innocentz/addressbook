﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AddressBook.EFRepository.Contacts;
using AddressBook.Models.Contacts;

namespace AddressBook.Services.Contacts
{
  public class ContactService : IContactService
  {
    private readonly IContactRepository _contactRepository;

    public ContactService(IContactRepository contactRepository)
    {
      _contactRepository = contactRepository;
    }

    public async Task AddContact(Contact contact)
    {
      await _contactRepository.AddContact(contact);
    }

    public async Task DeleteContact(int id)
    {
      await _contactRepository.Delete(id);
    }

    public async Task EditContact(Contact contact)
    {
      await _contactRepository.EditContact(contact);
    }

    public async Task<Contact> GetContact(int id)
    {
      return await _contactRepository.GetContact(id);
    }

    public async Task<IEnumerable<Contact>> GetContacts()
    {
      return await _contactRepository.GetContacts();
    }

    public async Task<IEnumerable<Contact>> SearchContacts(string query)
    {
		var results = await _contactRepository.GetContacts();
			return results.ToList().Where(x => ((x.Firstname.ToLower() + " " + x.LastName.ToLower()).Contains(query?.ToLower()) || string.IsNullOrEmpty(query)));
    }
  }
}
