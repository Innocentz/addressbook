﻿using AddressBook.Models.Contacts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AddressBook.Services.Contacts
{
    public interface IEmailAddressService
    {
        Task AddEmailAddress(EmailAddress emailAddress);
        Task EditEmailAddress(EmailAddress emailAddress);
        Task<EmailAddress> getEmailAddress(int Id);
        Task DeleteEmailAddress(int id);
    }
}
