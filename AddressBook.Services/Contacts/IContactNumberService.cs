﻿using AddressBook.Models.Contacts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AddressBook.Services.Contacts
{
    public interface IContactNumberService
    {
        Task AddContactNumber(ContactNumber contactNumber);
        Task EditContactNumber(ContactNumber contactNumber);
        Task<ContactNumber> getContactNumber(int Id);
        Task DeleteContactNumber(int id);
    }
}
