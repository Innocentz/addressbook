﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AddressBook.EFRepository.Contacts;
using AddressBook.Models.Contacts;

namespace AddressBook.Services.Contacts
{
    public class ContactNumberService : IContactNumberService
    {
        private readonly IContactNumberRepository _contactNumberRepository;

        public ContactNumberService(IContactNumberRepository contactNumberRepository)
        {
            _contactNumberRepository = contactNumberRepository;
        }
        public async Task AddContactNumber(ContactNumber contactNumber)
        {
            await _contactNumberRepository.AddContactNumber(contactNumber);
        }

        public async Task DeleteContactNumber(int Id)
        {
            await _contactNumberRepository.DeleteContactNumber(Id);
        }

        public async Task EditContactNumber(ContactNumber contactNumber)
        {
            await _contactNumberRepository.EditContactNumber(contactNumber);
        }

        public async Task<ContactNumber> getContactNumber(int Id)
        {
            return await _contactNumberRepository.getContactNumber(Id);
        }
    }
}
