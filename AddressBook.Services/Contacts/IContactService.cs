﻿using AddressBook.Models.Contacts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AddressBook.Services.Contacts
{
  public interface IContactService
  {
    Task<IEnumerable<Contact>> GetContacts();
    Task AddContact(Contact contact);
    Task EditContact(Contact contact);
    Task DeleteContact(int id);
    Task<Contact> GetContact(int id);
    Task<IEnumerable<Contact>> SearchContacts(string query);
  }
}
