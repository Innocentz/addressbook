﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AddressBook.EFRepository.Contacts;
using AddressBook.Models.Contacts;

namespace AddressBook.Services.Contacts
{
    public class EmailAddressService : IEmailAddressService
    {
        private readonly IEmailAddressRepository _emailAddressRepository;

        public EmailAddressService(IEmailAddressRepository emailAddressRepository)
        {
            _emailAddressRepository = emailAddressRepository;
        }
        public async Task AddEmailAddress(EmailAddress emailAddress)
        {
            await _emailAddressRepository.AddEmailAddress(emailAddress);
        }

        public async Task DeleteEmailAddress(int id)
        {
            await _emailAddressRepository.DeleteEmailAddress(id);
        }

        public async Task EditEmailAddress(EmailAddress emailAddress)
        {
            await _emailAddressRepository.EditEmailAddress(emailAddress);
        }

        public async Task<EmailAddress> getEmailAddress(int Id)
        {
            return await _emailAddressRepository.getEmailAddress(Id);
        }
    }
}
