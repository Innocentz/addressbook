﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AddressBook.Models.Contacts
{
    public class EmailAddress : IEntity
    {
        [Key]
        public int Id { get; set; }
        public int ContactId { get; set; }
        public string Email { get; set; }
        public bool Active { get; set; }

        [ForeignKey("ContactId")]
        public virtual Contact Contact { get; set; }
    }
}