﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AddressBook.Models.Contacts
{
    public class ContactNumber : IEntity
    {
        [Key]
        public int Id { get; set; }
       
        public int ContactId { get; set; }
        public string Phonenumber { get; set; }  
        public bool Active { get; set; }

        [ForeignKey("ContactId")]
        public virtual Contact Contact { get; set; }
    }
}