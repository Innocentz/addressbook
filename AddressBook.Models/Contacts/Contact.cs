﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AddressBook.Models.Contacts
{
    public class Contact : IEntity
    {
        [Key]
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string LastName { get; set; }
        public bool Active { get; set; }
        public ICollection<ContactNumber> ContactNumbers { get; set; }
        public ICollection<EmailAddress> EmailAddresses { get; set; }
    }
}
