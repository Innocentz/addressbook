﻿namespace AddressBook.Models.Contacts
{
    public interface IEntity
	{
		int Id { get; set; }
        bool Active { get; set; }
    }
}
