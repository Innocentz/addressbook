﻿using AddressBook.EFRepository.Context;
using AddressBook.Models.Contacts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AddressBook.EFRepository.Contacts
{
    public class EmailAddressRepository : GenericRepository<EmailAddress>, IEmailAddressRepository
    {
        public EmailAddressRepository(AddressBookContext context) : base(context) { }

        public async Task AddEmailAddress(EmailAddress emailAddress)
        {
            await Create(emailAddress);
        }

        public async Task DeleteEmailAddress(int Id)
        {
            await Delete(Id);
        }

        public async Task EditEmailAddress(EmailAddress emailAddress)
        {
            await Update(emailAddress);
        }

        public async Task<EmailAddress> getEmailAddress(int Id)
        {
            return await GetById(Id);
        }
    }
}
