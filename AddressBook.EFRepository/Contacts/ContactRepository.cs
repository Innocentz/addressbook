﻿using Microsoft.EntityFrameworkCore;
using AddressBook.EFRepository.Context;
using AddressBook.Models.Contacts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AddressBook.EFRepository.Contacts
{
	public class ContactRepository : GenericRepository<Contact>, IContactRepository
	{
		public ContactRepository(AddressBookContext context) : base(context) 	{}

        public async Task AddContact(Contact contact)
        {
            await Create(contact);
        }     

        public async Task DeleteContact(int id)
        {
            await Delete(id);
        }
        public async Task EditContact(Contact contact)
        {
            await Update(contact);
        }
        public async Task<Contact> GetContact(int id)
        {
            return await GetById(id);
        }

        public async Task<IEnumerable<Contact>> GetContacts()
        {
            return await GetAll()
                .Include(x=>x.ContactNumbers)
                .Include(x=>x.EmailAddresses)
                .ToListAsync();
        }
	}
}
