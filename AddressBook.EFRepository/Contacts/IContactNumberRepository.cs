﻿using AddressBook.Models.Contacts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AddressBook.EFRepository.Contacts
{
    public interface IContactNumberRepository : IGenericRepository<ContactNumber>
    {
        Task AddContactNumber(ContactNumber contactNumber);
        Task EditContactNumber(ContactNumber contactNumber);
        Task<ContactNumber> getContactNumber(int Id);
        Task DeleteContactNumber(int id);
    }
}
