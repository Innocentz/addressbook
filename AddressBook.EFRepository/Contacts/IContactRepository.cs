﻿
using AddressBook.Models.Contacts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AddressBook.EFRepository.Contacts
{
	public interface IContactRepository : IGenericRepository<Contact>
	{
        Task<IEnumerable<Contact>> GetContacts();
        Task AddContact(Contact contact);
        Task EditContact(Contact contact);
        Task DeleteContact(int id);
        Task<Contact> GetContact(int id);
    }
}
