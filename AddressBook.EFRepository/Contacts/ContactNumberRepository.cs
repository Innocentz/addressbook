﻿using AddressBook.EFRepository.Context;
using AddressBook.Models.Contacts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AddressBook.EFRepository.Contacts
{
    public class ContactNumberRepository : GenericRepository<ContactNumber>, IContactNumberRepository
    {
        public ContactNumberRepository(AddressBookContext context) : base(context) { }
        public async Task AddContactNumber(ContactNumber contactNumber)
        {
           await Create(contactNumber);
        }

        public async Task DeleteContactNumber(int id)
        {
            await Delete(id);
        }

        public async Task EditContactNumber(ContactNumber contactNumber)
        {
            await Update(contactNumber);
        }

        public async Task<ContactNumber> getContactNumber(int Id)
        {
           return await GetById(Id);
        }
    }
}
