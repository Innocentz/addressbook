﻿using AddressBook.Models.Contacts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AddressBook.EFRepository.Contacts
{
    public interface IEmailAddressRepository : IGenericRepository<EmailAddress>
    {
        Task AddEmailAddress(EmailAddress emailAddress);
        Task EditEmailAddress(EmailAddress emailAddress);
        Task<EmailAddress> getEmailAddress(int Id);
        Task DeleteEmailAddress(int id);
    }
}
