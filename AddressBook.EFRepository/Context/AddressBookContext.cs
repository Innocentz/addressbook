﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using AddressBook.Models.Contacts;

namespace AddressBook.EFRepository.Context
{
    public class AddressBookContext : IdentityDbContext
    {
        public AddressBookContext(DbContextOptions<AddressBookContext> options)
          : base(options) { }

        public virtual DbSet<Contact> Contact { get; set; }
        public virtual DbSet<ContactNumber> ContactNumber { get; set; }
        public virtual DbSet<EmailAddress> EmailAddress { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
