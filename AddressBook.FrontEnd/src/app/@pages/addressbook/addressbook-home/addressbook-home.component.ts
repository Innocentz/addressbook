import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { ToasterService, ToasterConfig, Toast, BodyOutputType } from 'angular2-toaster';
import { ContactService } from '../../../shared/services/contacts.data.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { IContact, IContactViewModel, ContactType, DialogResult } from '../../../shared/interfaces/interfaces';
import { ContactAddDialogComponent } from '../contact-add-dialog/contact-add-dialog.component';
import { ContactDeleteDialogComponent } from '../contact-delete-dialog/contact-delete-dialog.component';
import { ContactViewComponent } from '../contact-view/contact-view.component';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-addressbook-home',
  templateUrl: './addressbook-home.component.html',
  styleUrls: ['./addressbook-home.component.scss']
})
export class AddressbookHomeComponent implements OnInit {

  searchTerm$ = new Subject<string>();
  public showLoader: boolean = false;
  public contacts: IContact[];

  constructor(
    public http: Http,
    private toasterService: ToasterService,
    public contactService: ContactService,
    // private configService: ConfigService,
    public router: Router,
    public dialog: MatDialog
  ) {
     this.contactService.Search(this.searchTerm$)
    .subscribe((results: any) => {
       this.contacts = results;
    });
  }

  config: ToasterConfig;
  position: string = "toast-top-full-width";
  animationType: string = "slideDown";
  title: string = "HI there!";
  content: string = `I'm cool toaster!`;
  timeout: number = 5000;
  toastsLimit: number = 5;
  type: string = "default";
  isNewestOnTop: boolean = true;
  isHideOnClick: boolean = true;
  isDuplicatesPrevented: boolean = false;
  isCloseButton: boolean = true;

  ngOnInit() {
    this.getAllContacts();
  }

  getAllContacts() {
    this.showLoader = true;
    this.contactService.GetAllContacts().subscribe(
      (result: any) => {
        this.contacts = result;
        this.showLoader = false;
        },
      error => {
        this.showToast(
          "error",
          "Contacts Error",
          "Error occurred while Loading contacts " + error
        );
      }
    );
  }

  CreateContact(): void {
    let contactmodel: IContactViewModel = {
      id: 0,
      firstname: '',
      lastName: ''
    };
    const dialogRef = this.dialog.open(ContactAddDialogComponent, {
      width: '25%',
      data: { ContactModel: contactmodel}
    });
    dialogRef.afterClosed().subscribe(result => {
      contactmodel = result;
        if(contactmodel) {
          this.addContact(contactmodel);
        }
    });
  }

  EditContact(contact: IContact): void {
    let contactmodel: IContactViewModel = {
      id: contact.id,
      firstname: contact.firstname,
      lastName: contact.lastName
    };

    const dialogRef = this.dialog.open(ContactAddDialogComponent, {
      width: '25%',
      data: { ContactModel: contactmodel }
    });

    dialogRef.afterClosed().subscribe(result => {
      contactmodel = result;
        if(contactmodel) {
          this.editContact(contactmodel);
        }
    });
  }

  reload() {
    this.getAllContacts();
  }

  DeleteContact(contact: IContact) {
    const dialogRef = this.dialog.open(ContactDeleteDialogComponent, {
      data: { contactType: ContactType.Contact }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if(result === DialogResult.OK) {
        this.removeContact(contact.id);
      }
    });
  }

  ViewContact(contact: any) {
    const dialogRef = this.dialog.open(ContactViewComponent, {
      data: { contact: contact }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.reload();
    });
  }

  addContact(contact: IContactViewModel) {
    this.contactService.AddContact(contact).subscribe(
      (result: any) => {
        this.showLoader = false;
        console.log(result);
        this.reload();
        },
      error => {
        this.showToast(
          "error",
          "Contacts Error",
          "Error occurred while creating Contact " + error
        );
      }
    );
  }

  editContact(contact: IContactViewModel) {
    this.contactService.EditContact(contact).subscribe(
      (result: any) => {
        this.showLoader = false;
        console.log(result);
        this.reload();
        },
      error => {
        this.showToast(
          "error",
          "Contacts Error",
          "Error occurred while updating Contact " + error
        );
      }
    );
  }

  removeContact(id: number) {
    this.contactService.DeleteContact(id).subscribe(
      (result: any) => {
        this.showLoader = false;
        console.log(result);
        this.reload();
        },
      error => {
        this.showToast(
          "error",
          "Contacts Error",
          "Error occurred while delete Contact " + error
        );
      }
    );
  }

  makeToast() {
    this.showToast(this.type, this.title, this.content);
  }

  private showToast(type: string, title: string, body: string) {
    this.config = new ToasterConfig({
      positionClass: this.position,
      timeout: this.timeout,
      newestOnTop: this.isNewestOnTop,
      tapToDismiss: this.isHideOnClick,
      preventDuplicates: this.isDuplicatesPrevented,
      animation: this.animationType,
      limit: this.toastsLimit
    });
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: this.timeout,
      showCloseButton: this.isCloseButton,
      bodyOutputType: BodyOutputType.TrustedHtml
    };
    this.toasterService.popAsync(toast);
  }
}
