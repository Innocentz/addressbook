import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddressbookHomeComponent } from './addressbook-home.component';

describe('AddressbookHomeComponent', () => {
  let component: AddressbookHomeComponent;
  let fixture: ComponentFixture<AddressbookHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddressbookHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddressbookHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
