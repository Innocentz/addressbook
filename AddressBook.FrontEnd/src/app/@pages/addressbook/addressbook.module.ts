import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddressbookHomeComponent } from './addressbook-home/addressbook-home.component';
import { Routes, RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatIconModule, MatCardModule, MatDialogModule, MatTabsModule, MatToolbarModule, MatListModule, MatMenuModule, MatChipsModule, MatProgressBarModule, MatFormFieldModule, MatButtonToggleModule, MatInputModule, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { Ng2OdometerModule } from 'ng2-odometer';
import { RoundProgressModule } from 'angular-svg-round-progressbar';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { ContactService } from '../../shared/services/contacts.data.service';
import { ContactAddDialogComponent } from './contact-add-dialog/contact-add-dialog.component';
import { ContactDeleteDialogComponent } from './contact-delete-dialog/contact-delete-dialog.component';
import { ContactViewComponent } from './contact-view/contact-view.component';
import { AddphoneDialogComponent } from './contact-view/addphone-dialog/addphone-dialog.component';
import { AddemailDialogComponent } from './contact-view/addemail-dialog/addemail-dialog.component';

export const appRoutes: Routes = [{ path: "", component: AddressbookHomeComponent }];

@NgModule({
  declarations: [
    AddressbookHomeComponent,
    ContactAddDialogComponent,
    ContactDeleteDialogComponent,
    ContactViewComponent,
    AddphoneDialogComponent,
    AddemailDialogComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatDialogModule,
    MatTabsModule,
    MatToolbarModule,
    MatListModule,
    Ng2OdometerModule,
    RoundProgressModule,
    MatMenuModule,
    MatChipsModule,
    MatProgressBarModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatButtonToggleModule,
    MatInputModule,
    RouterModule.forChild(appRoutes),
    FormsModule,
    ToasterModule
  ],
  exports: [
    AddressbookHomeComponent,

    // CategoryAlertDialogComponent
  ],
  providers: [
    ToasterService,
    ContactService,
    { provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: true } }
  ],
  entryComponents: [
    ContactAddDialogComponent,
    ContactDeleteDialogComponent,
    ContactViewComponent,
    AddphoneDialogComponent,
    AddemailDialogComponent
  ]
})
export class AddressbookModule { }
