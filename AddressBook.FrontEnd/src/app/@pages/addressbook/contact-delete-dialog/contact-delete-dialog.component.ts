import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ContactType, DialogResult } from '../../../shared/interfaces/interfaces';

@Component({
  selector: 'app-contact-delete-dialog',
  templateUrl: './contact-delete-dialog.component.html',
  styleUrls: ['./contact-delete-dialog.component.scss']
})
export class ContactDeleteDialogComponent implements OnInit {

  Title: string;
  contactType: ContactType;
  constructor(public dialogRef: MatDialogRef<ContactDeleteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.contactType = data.contactType;
    }

  ngOnInit() {
    this.setTitle();
  }

  setTitle() {
    switch(this.contactType)
    {
      case ContactType.Contact:
         this.Title = "Contact";
         break;
      case ContactType.ContactNumber:
      this.Title = "Contact Number";
         break;
      case ContactType.Email:
        this.Title = "Email Address";
        break;
        default: this.Title = "Contact";
    }
  }

  onOkclick() {
    this.dialogRef.close(DialogResult.OK);
  }

  onClose() {
    this.dialogRef.close(DialogResult.Cancel);
  }

}
