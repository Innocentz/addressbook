import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { IContact, IContactViewModel } from '../../../shared/interfaces/interfaces';

@Component({
  selector: 'app-contact-add-dialog',
  templateUrl: './contact-add-dialog.component.html',
  styleUrls: ['./contact-add-dialog.component.scss']
})
export class ContactAddDialogComponent implements OnInit {

  contactForm: FormGroup;
  contact: IContactViewModel;
  Title: string;

  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<ContactAddDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.contact = this.data.ContactModel;
    this.Title = this.contact.id === 0 ? "Create" : "Edit";

    this.contactForm = this.fb.group({
        firstname: [this.contact.firstname , Validators.required],
        lastName: [this.contact.lastName, Validators.required],
     });
  }

  onSubmit() {
    this.contact = {
      firstname: this.contactForm.value["firstname"],
      lastName: this.contactForm.value["lastName"],
      id: this.contact.id
    }
    this.dialogRef.close(this.contact);
  }

  onClose() {
    this.dialogRef.close();
  }
}
