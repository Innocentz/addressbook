import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { IContact, IContactNumber, IEmailAddress, ContactType, DialogResult } from '../../../shared/interfaces/interfaces';
import { AddphoneDialogComponent } from './addphone-dialog/addphone-dialog.component';
import { ContactService } from '../../../shared/services/contacts.data.service';
import { ContactDeleteDialogComponent } from '../contact-delete-dialog/contact-delete-dialog.component';
import { AddemailDialogComponent } from './addemail-dialog/addemail-dialog.component';

@Component({
  selector: 'app-contact-view',
  templateUrl: './contact-view.component.html',
  styleUrls: ['./contact-view.component.scss']
})
export class ContactViewComponent implements OnInit {

  contact: IContact;
  contactNumbers: IContactNumber[];
  emailAddress: IEmailAddress[];

  constructor(public dialogRef: MatDialogRef<ContactViewComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog,
    public contactService: ContactService,
    ) {
      this.contact = this.data.contact;
      this.contactNumbers = this.contact.contactNumbers;
      this.emailAddress = this.contact.emailAddresses;
    }

  ngOnInit() {
  }

  AddPhoneNumber() {
    let contactmodel: IContactNumber = {
      id: 0,
      phonenumber: '',
      contactId: this.contact.id
    };
    const dialogRef = this.dialog.open(AddphoneDialogComponent, {
      width: '25%',
      data: { phonenumber: contactmodel}
    });
    dialogRef.afterClosed().subscribe(result => {
      contactmodel = result;
        if(contactmodel) {
          this.onClose();
          this.insertPhonenumber(contactmodel);
        }
    });
  }

  EditPhoneNumber(phonenumber: IContactNumber) {
    const dialogRef = this.dialog.open(AddphoneDialogComponent, {
       width: '25%',
      data: { phonenumber: phonenumber}
    });
    dialogRef.afterClosed().subscribe(result => {
        phonenumber = result;
        if(phonenumber) {
          this.onClose();
          this.UpdatePhonenumber(phonenumber);
        }
    });
  }

  AddEmailAddress() {
    let contactmodel: IEmailAddress = {
      id: 0,
      email: '',
      contactId: this.contact.id
    };
    const dialogRef = this.dialog.open(AddemailDialogComponent, {
      width: '25%',
      data: { emailAddress: contactmodel}
    });
    dialogRef.afterClosed().subscribe(result => {
      contactmodel = result;
      console.log('Email address');
      console.log(contactmodel);
        if(contactmodel) {
          this.onClose();
          this.insertEmail(contactmodel);
        }
    });
  }

  EditEmailAddress(emailAddress: IEmailAddress) {
    const dialogRef = this.dialog.open(AddemailDialogComponent, {
      width: '25%',
     data: { emailAddress: emailAddress}
   });
   dialogRef.afterClosed().subscribe(result => {
    emailAddress = result;
       if(emailAddress) {
         this.onClose();
         this.updateEmail(emailAddress);
       }
   });
  }

  insertEmail(contactmodel: IEmailAddress): any {
     this.contactService.AddEmailAddress(contactmodel).subscribe(
      (result: any) => {
        console.log(result);
        },
      error => {
        console.log(error);
      }
    );
  }

  updateEmail(contactmodel: IEmailAddress): any {
    this.contactService.UpdateEmailAddress(contactmodel).subscribe(
     (result: any) => {
       console.log(result);
       },
     error => {
       console.log(error);
     }
   );
 }

 DeleteEmail(contactmodel: IEmailAddress): any {
  const dialogRef = this.dialog.open(ContactDeleteDialogComponent, {
    data: { contactType: ContactType.Email }
  });

  dialogRef.afterClosed().subscribe(result => {
    console.log(result);
    if(result === DialogResult.OK) {
      this.onClose();
      this.removeEmail(contactmodel.id);
    }
  });
}
  removeEmail(id: number): any {
    this.contactService.DeleteEmail(id).subscribe(
      (result: any) => {
        console.log(result);
        },
      error => {
        console.log(error);
      }
    );
  }

  insertPhonenumber(Phonenumber: IContactNumber) {
    this.contactService.AddPhone(Phonenumber).subscribe(
      (result: any) => {
        console.log(result);
        },
      error => {
        console.log(error);
      }
    );
  }

  DeletePhoneNumber(Phonenumber: IContactNumber) {
    const dialogRef = this.dialog.open(ContactDeleteDialogComponent, {
      data: { contactType: ContactType.ContactNumber }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if(result === DialogResult.OK) {
        this.onClose();
        this.removeContact(Phonenumber.id);
      }
    });
  }

  removeContact(id: any): any {
    this.contactService.DeletePhoneNumber(id).subscribe(
      (result: any) => {
        console.log(result);
        },
      error => {
        console.log(error);
      }
    );
  }

  UpdatePhonenumber(phonenumber: IContactNumber) {
    this.contactService.updatePhone(phonenumber).subscribe(
      (result: any) => {
        console.log(result);
        },
      error => {
        console.log(error);
      }
    );
  }

  onClose() {
    this.dialogRef.close();
  }
}
