import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddphoneDialogComponent } from './addphone-dialog.component';

describe('AddphoneDialogComponent', () => {
  let component: AddphoneDialogComponent;
  let fixture: ComponentFixture<AddphoneDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddphoneDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddphoneDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
