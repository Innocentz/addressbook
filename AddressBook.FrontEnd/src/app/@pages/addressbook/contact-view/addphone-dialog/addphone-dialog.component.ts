import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ContactAddDialogComponent } from '../../contact-add-dialog/contact-add-dialog.component';
import { IContactNumber } from '../../../../shared/interfaces/interfaces';

@Component({
  selector: 'app-addphone-dialog',
  templateUrl: './addphone-dialog.component.html',
  styleUrls: ['./addphone-dialog.component.scss']
})
export class AddphoneDialogComponent implements OnInit {

  phoneForm: FormGroup;
  Title: string;
  phonenumber: IContactNumber;
  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<ContactAddDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.phonenumber = this.data.phonenumber;
    this.Title = this.phonenumber.id === 0 ? "Add" : "Edit";

    this.phoneForm = this.fb.group({
      phonenumber: [this.phonenumber.phonenumber , Validators.required],
     });
  }

  onSubmit() {
    this.phonenumber.phonenumber = this.phoneForm.get('phonenumber').value;
    this.dialogRef.close(this.phonenumber);
  }

  onClose() {
    this.dialogRef.close();
  }
}
