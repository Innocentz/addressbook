import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ContactAddDialogComponent } from '../../contact-add-dialog/contact-add-dialog.component';
import { IEmailAddress } from '../../../../shared/interfaces/interfaces';

@Component({
  selector: 'app-addemail-dialog',
  templateUrl: './addemail-dialog.component.html',
  styleUrls: ['./addemail-dialog.component.scss']
})
export class AddemailDialogComponent implements OnInit {

  emailAddress: IEmailAddress;
  emailForm: FormGroup;
  Title: string;

  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<AddemailDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.emailAddress = this.data.emailAddress;
    this.Title = this.emailAddress.id === 0 ? "Add" : "Edit";

    this.emailForm = this.fb.group({
      email: [this.emailAddress.email , Validators.required],
     });
  }

  onSubmit() {
    this.emailAddress.email = this.emailForm.get('email').value;
    this.dialogRef.close(this.emailAddress);
  }

  onClose() {
    this.dialogRef.close();
  }

}
