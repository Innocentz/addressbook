import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddemailDialogComponent } from './addemail-dialog.component';

describe('AddemailDialogComponent', () => {
  let component: AddemailDialogComponent;
  let fixture: ComponentFixture<AddemailDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddemailDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddemailDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
