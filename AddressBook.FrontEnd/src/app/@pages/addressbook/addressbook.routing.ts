import { Routes, RouterModule } from "@angular/router";
import { AddressbookHomeComponent } from "./addressbook-home/addressbook-home.component";


const childRoutes: Routes = [
  {
    path: "contacts",
    component: AddressbookHomeComponent
  }
];

export const routing = RouterModule.forChild(childRoutes);