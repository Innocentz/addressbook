import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import { IContact, IContactViewModel, IContactNumber, IEmailAddress } from "../interfaces/interfaces";
import { ConfigService } from "../utils/config.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable()
export class ContactService {

  _baseUrl: string = "";

  constructor(private http: HttpClient, private configService: ConfigService) {
    this._baseUrl = configService.getApiURI();
  }

  Search(query: Observable<string>) {
    return query.debounceTime(400)
    .distinctUntilChanged()
    .switchMap(term => this.searchEntries(term));
  }

  searchEntries(query): Observable<IContact> {
    return this.http
        .get(this._baseUrl + "Contact/Search/" + query)
        .map((res: IContact) => res)
        .catch(this.handleError);
  }

  GetAllContacts(): Observable<IContact> {
    const headers = new HttpHeaders();
    headers.append("Content-Type", "application/json");
    return this.http
      .get(this._baseUrl + "Contact/", {
        headers: headers
      })
      .map((res: IContact) => res)
      .catch(this.handleError);
  }

  GetContact(id: number): Observable<IContact> {
    const headers = new HttpHeaders();
    headers.append("Content-Type", "application/json");
    return this.http
      .get(this._baseUrl + "Contact/"+ id, {
        headers: headers
      })
      .map((res: IContact) => res)
      .catch(this.handleError);
  }


  AddContact(contact: IContactViewModel): Observable<any> {
    const headers = new HttpHeaders();
    headers.append("Content-Type", "application/json");
    return this.http
      .post(this._baseUrl + "Contact/", contact, {
        headers: headers
      })
      .map((res: any) => res)
      .catch(this.handleError);
  }

  AddPhone(phonenumber: IContactNumber): Observable<any> {
    const headers = new HttpHeaders();
    headers.append("Content-Type", "application/json");
    return this.http
      .post(this._baseUrl + "ContactNumber/", phonenumber, {
        headers: headers
      })
      .map((res: any) => res)
      .catch(this.handleError);
  }

  DeletePhoneNumber(id: number) {
    const headers = new HttpHeaders();
    headers.append("Content-Type", "application/json");
    return this.http
      .delete(this._baseUrl + "ContactNumber/" + id, {
        headers: headers
      })
      .map((res: any) => res)
      .catch(this.handleError);
  }

  EditContact(contact: IContactViewModel): Observable<any> {
    const headers = new HttpHeaders();
    headers.append("Content-Type", "application/json");
    return this.http
      .put(this._baseUrl + "Contact/", contact, {
        headers: headers
      })
      .map((res: any) => res)
      .catch(this.handleError);
  }

  updatePhone(phonenumber: IContactNumber): Observable<any> {
    const headers = new HttpHeaders();
    headers.append("Content-Type", "application/json");
    return this.http
      .put(this._baseUrl + "ContactNumber/", phonenumber, {
        headers: headers
      })
      .map((res: any) => res)
      .catch(this.handleError);
  }

  DeleteContact(id: number):  Observable<any> {
    const headers = new HttpHeaders();
    headers.append("Content-Type", "application/json");
    return this.http
      .delete(this._baseUrl + "Contact/" + id, {
        headers: headers
      })
      .map((res: any) => res)
      .catch(this.handleError);
  }

  AddEmailAddress(contactmodel: IEmailAddress): Observable<any> {
    const headers = new HttpHeaders();
    headers.append("Content-Type", "application/json");
    return this.http
      .post(this._baseUrl + "EmailAddress/", contactmodel, {
        headers: headers
      })
      .map((res: any) => res)
      .catch(this.handleError);
  }

  DeleteEmail(id: number): Observable<any> {
    const headers = new HttpHeaders();
    headers.append("Content-Type", "application/json");
    return this.http
      .delete(this._baseUrl + "EmailAddress/" + id, {
        headers: headers
      })
      .map((res: any) => res)
      .catch(this.handleError);
  }

  UpdateEmailAddress(contactmodel: IEmailAddress): Observable<any> {
    const headers = new HttpHeaders();
    headers.append("Content-Type", "application/json");
    return this.http
      .put(this._baseUrl + "EmailAddress/", contactmodel, {
        headers: headers
      })
      .map((res: any) => res)
      .catch(this.handleError);
  }

  private handleError(error: any) {
    const applicationError = error.headers.get("Application-Error");
    console.error(error.message || error);
    return Observable.throw(error.message || error);
  }
}
