import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";


const routes: Routes = [
  //{ path: "auth", loadChildren: "../auth/auth.module#AuthModule" },
  {
    path: "addressbook",
        loadChildren:
          "../../../@pages/addressbook/addressbook.module#AddressbookModule"
  },
  {
    path: "registers",
    loadChildren:
      "../../../@pages/pages/registernew/register.module#RegisterSModule"
  },
  {
    path: "logins",
    loadChildren: "../../../@pages/pages/loginnew/login.module#LoginSModule"
  },
  {
    path: "logins",
    loadChildren: "../../../@pages/pages/loginnew/login.module#LoginSModule"
  },

  { path: "**", redirectTo: "addressbook" }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { useHash: true, enableTracing: true })
  ],
  exports: [RouterModule]
})
export class LazyLoadModule {}
