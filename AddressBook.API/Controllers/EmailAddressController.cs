﻿using AddressBook.API.Helpers.Contracts;
using AddressBook.API.ViewModels.Contacts;
using AddressBook.Models.Contacts;
using AddressBook.Services.Contacts;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace AddressBook.API.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class EmailAddressController : ControllerBase
	{
		private readonly IMapper _mapper;
		private readonly IActionResponseHelper _actionResponseHelper;
		private readonly IEmailAddressService _emailAddressService;

		public EmailAddressController(IMapper mapper, IActionResponseHelper actionResponseHelper, IEmailAddressService emailAddressService)
		{
			_mapper = mapper;
			_actionResponseHelper = actionResponseHelper;
			_emailAddressService = emailAddressService;
		}

		[HttpGet("{id}")]
		public async Task<IActionResult> Get(int id)
		{
			var result = await _emailAddressService.getEmailAddress(id);
			return _actionResponseHelper.GetTResult<EmailAddressViewModel, EmailAddress>(result);
		}

		[HttpPost]
		public async Task<IActionResult> Post([FromBody] EmailAddressViewModel model)
		{
			var contact = _mapper.Map<EmailAddress>(model);
			await _emailAddressService.AddEmailAddress(contact);
			return Ok(new { message = $"{ model.Email } has been added!" });
		}

		[HttpPut]
		public async Task<IActionResult> Put([FromBody] EmailAddressViewModel model)
		{
			var contact = _mapper.Map<EmailAddress>(model);
			await _emailAddressService.EditEmailAddress(contact);
			return Ok(new { message = $"email addredd has been updated!" });
		}

		[HttpDelete("{id}")]
		public async Task<IActionResult> Delete(int id)
		{
			await _emailAddressService.DeleteEmailAddress(id);
			return Ok(new { message = $"Email Address has been added!" });
		}
	}
}