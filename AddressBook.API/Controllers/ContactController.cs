﻿using AddressBook.API.Helpers.Contracts;
using AddressBook.API.ViewModels.Contacts;
using AddressBook.Models.Contacts;
using AddressBook.Services.Contacts;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AddressBook.API.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class ContactController : ControllerBase
	{
		private readonly IMapper _mapper;
		private readonly IActionResponseHelper _actionResponseHelper;
		private readonly IContactService _contactService;
		public ContactController(IMapper mapper, IActionResponseHelper actionResponseHelper, IContactService contactService)
		{
			_mapper = mapper;
			_actionResponseHelper = actionResponseHelper;
			_contactService = contactService;
		}

		[HttpGet]
		public async Task<IActionResult> Get()
		{
			var result = await _contactService.GetContacts();
			return _actionResponseHelper.GetTResult<List<ContactViewModel>, IEnumerable<Contact>>(result);
		}

		[HttpGet("{id}")]
		public async Task<IActionResult> Get(int id)
		{
			var result = await _contactService.GetContact(id);
			return _actionResponseHelper.GetTResult<ContactViewModel, Contact>(result);
		}

		[HttpGet("Search/{query}")]
		public async Task<IActionResult> Get(string query)
		{
			var result = await _contactService.SearchContacts(query);
			return _actionResponseHelper.GetTResult<List<ContactViewModel>, IEnumerable<Contact>>(result);
		}

		[HttpPost]
		public async Task<IActionResult> Post([FromBody] AddContactViewModel model)
		{
			var contact = _mapper.Map<Contact>(model);
			await _contactService.AddContact(contact);
			return Ok(new { message = $"{ model.Firstname } has been added!" });
		}

		[HttpPut]
		public async Task<IActionResult> Put([FromBody] AddContactViewModel model)
		{
			var contact = _mapper.Map<Contact>(model);
			await _contactService.EditContact(contact);
			return Ok(new { message = $"Contact has been updated!" });
		}

		[HttpDelete("{id}")]
		public async Task<IActionResult> Delete(int id)
		{
			await _contactService.DeleteContact(id);
			return Ok(new { message = $"Contact number has been deleted!" });
		}

	}
}