﻿using AddressBook.API.Helpers.Contracts;
using AddressBook.API.ViewModels.Contacts;
using AddressBook.Models.Contacts;
using AddressBook.Services.Contacts;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AddressBook.API.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class ContactNumberController : ControllerBase
	{
		private readonly IMapper _mapper;
		private readonly IActionResponseHelper _actionResponseHelper;
		private readonly IContactNumberService _contactNumberService;

		public ContactNumberController(IMapper mapper, IActionResponseHelper actionResponseHelper, IContactNumberService contactNumberService)
		{
			_mapper = mapper;
			_actionResponseHelper = actionResponseHelper;
			_contactNumberService = contactNumberService;
		}

		[HttpGet("{id}")]
		public async Task<IActionResult> Get(int id)
		{
			var result = await _contactNumberService.getContactNumber(id);
			return _actionResponseHelper.GetTResult<ContactNumberViewModel, ContactNumber>(result);
		}

		[HttpPost]
		public async Task<IActionResult> Post([FromBody] ContactNumberViewModel model)
		{
			var contact = _mapper.Map<ContactNumber>(model);
			await _contactNumberService.AddContactNumber(contact);
			return Ok(new { message = $"{ model.Phonenumber } has been added!" });
		}

		[HttpPut]
		public async Task<IActionResult> Put([FromBody] ContactNumberViewModel model)
		{
			var contact = _mapper.Map<ContactNumber>(model);
			await _contactNumberService.EditContactNumber(contact);
			return Ok(new { message = $"phone number has been updated!" });
		}

		[HttpDelete("{id}")]
		public async Task<IActionResult> Delete(int id)
		{
			await _contactNumberService.DeleteContactNumber(id);
			return Ok(new { message = $"Phone number has been deleted!" });
		}
	}
}