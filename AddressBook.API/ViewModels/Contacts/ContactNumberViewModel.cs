﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AddressBook.API.ViewModels.Contacts
{
	public class ContactNumberViewModel
	{
		public int Id { get; set; }
		[Required]
		public int ContactId { get; set; }
		[Required]
		public string Phonenumber { get; set; }
	}
}
