﻿using AddressBook.Models.Contacts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AddressBook.API.ViewModels.Contacts
{
	public class AddContactViewModel
	{
		public int Id { get; set; }
		[Required]
		public string Firstname { get; set; }
		[Required]
		public string LastName { get; set; }
	}
}
