﻿using AddressBook.Models.Contacts;
using System.Collections.Generic;

namespace AddressBook.API.ViewModels.Contacts
{
    public class ContactViewModel
    {
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string LastName { get; set; }
        public IEnumerable<ContactNumberViewModel> ContactNumbers { get; set; }
        public IEnumerable<EmailAddressViewModel> EmailAddresses { get; set; }
    }
}
