﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using AddressBook.API.Helpers.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AddressBook.API.Helpers
{
	public class ActionResponseHelper : ControllerBase, IActionResponseHelper
	{
		private readonly IMapper _mapper;
		public ActionResponseHelper(IMapper mapper) => _mapper = mapper;

		public IActionResult GetTResult<TModel, TResult>(TResult result)
		{
			if (result != null)
			{
				var returnResult = _mapper.Map<TModel>(result);
				return Ok(returnResult);
			}
			return NoContent();
		}

		public IActionResult GetTResult<TModel, TResult>(TResult result, string message = "")
		{
			if (result != null)
			{
				var returnResult = _mapper.Map<TModel>(result);
				return Ok(returnResult);
			}
			return BadRequest(new { message });
		}
	}
}
