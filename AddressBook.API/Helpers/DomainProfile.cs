﻿using AddressBook.API.ViewModels.Contacts;
using AddressBook.Models.Contacts;
using AutoMapper;
using System.Collections.Generic;


namespace AddressBook.API.Helpers
{
	public class DomainProfile : Profile
	{

		public DomainProfile()
		{
			CreateMap<Contact, ContactViewModel>();
			CreateMap<AddContactViewModel, Contact>()
				 .ForMember(x => x.Active, opt => opt.MapFrom(x => true))
				 .ForMember(x => x.ContactNumbers, opt => opt.Ignore())
				 .ForMember(x => x.EmailAddresses, opt => opt.Ignore());
			CreateMap<EmailAddressViewModel, EmailAddress>()
					.ForMember(x => x.Contact, opt => opt.Ignore())
					.ForMember(x => x.Active, opt => opt.MapFrom(x => true));
			CreateMap<ContactNumberViewModel, ContactNumber>()
					.ForMember(x => x.Contact, opt => opt.Ignore())
					.ForMember(x => x.Active, opt => opt.MapFrom(x => true));
		}
	}
}
