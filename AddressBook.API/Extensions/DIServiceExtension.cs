﻿using Microsoft.Extensions.DependencyInjection;
using AddressBook.API.Helpers;
using AddressBook.API.Helpers.Contracts;
using AddressBook.EFRepository.Contacts;
using AddressBook.Services.Contacts;

namespace AddressBook.API.Extensions
{
	public static class DIServiceExtension
	{
		public static void AddDIServices(this IServiceCollection services)
		{
			services.AddTransient<IContactRepository, ContactRepository>();
			services.AddTransient<IContactService, ContactService>(); 
			services.AddTransient<IActionResponseHelper, ActionResponseHelper>();
            services.AddTransient<IContactNumberService, ContactNumberService>();
            services.AddTransient<IContactNumberRepository, ContactNumberRepository>();
            services.AddTransient<IEmailAddressRepository, EmailAddressRepository>();
            services.AddTransient<IEmailAddressService, EmailAddressService>();
        }
	}
}
